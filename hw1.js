// 1. Переменная let отличается от var тем, что let не видно за пределами 
// блока и она видна только после объявления. Переменная var - устарела, её
// можно объявлять в любом месте кода, до объявления она будет равна 
// undefined, её видно по всему коду. Переменная 
// const - это константа, её значение не меняется.
// 2. Объявлять переменную var считается плохим тоном потому, что это 
// устаревшая переменная, использование её для объявления переменной
// приводит к ошибкам и конфликтам в коде.

let userName = prompt ('Введите Ваше имя.');
let userAge = +prompt ('Введите Ваш возраст.');

if (userAge < 18) {
     alert ('You are not allowed to visit this website');
} else if (userAge >= 18 && userAge <= 22) {
    userResponse = confirm ('Are you sure you want to continue?');
      if (userResponse === true) {
        alert (`Welcome, ${userName}`);
        } else {
        alert ('You are not allowed to visit this website')
        } 
} else if (userAge > 22) {
    alert (`Welcome, ${userName}`)
}